package com.omi.oi.goeuro.busroute.repository;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.omi.oi.goeuro.busroute.exception.BusRouteException;
import com.omi.oi.goeuro.busroute.importer.BusRoutesLocalFileImporter;
import com.omi.oi.goeuro.busroute.model.BusRoute;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class BusRouteRepositoryTest {

  @Autowired
  private BusRoutesLocalFileImporter importer;

  @Autowired
  private BusRouteRepository busRouteRepository;

  @Before
  public void setup() throws Exception {
    importer.loadDataFile("data/busroutesdata.txt");
  }


  @Test
  public void testGetById() {
    BusRoute busRoute = busRouteRepository.getById(1);
    Assert.assertTrue(busRoute.getId() == 1);
    Assert.assertTrue(busRoute.getRoutes().contains(24));
  }

  @Test(expected = IllegalStateException.class)
  public void testGetByIdDoesNotExist() {
    BusRoute busRoute = busRouteRepository.getById(100);
  }

  @Test
  public void tetGetAll() throws BusRouteException {
    int sizeBefore = busRouteRepository.getAll().size();
    busRouteRepository.add(100, new HashSet<>(Arrays.asList(5, 7, 9, 2)));
    Assert.assertTrue(busRouteRepository.getAll().size() > sizeBefore);
    Assert.assertTrue(busRouteRepository.getById(100).getId() == 100);
  }

  @Test(expected = BusRouteException.class)
  public void tetGetAllIdAlreadyExists() throws BusRouteException {
    busRouteRepository.add(1, new HashSet<>(Arrays.asList(5, 7, 9, 2)));
  }

  @Test(expected = UnsupportedOperationException.class)
  public void tetGetAllImmutableRoutesList() throws BusRouteException {
    busRouteRepository.getAll().add(new BusRoute(1, new HashSet<>(Arrays.asList(1))));
  }

  @Test
  public void tetRemoveAll() throws BusRouteException {
    List<BusRoute> busRoutes = busRouteRepository.getAll();
    int sizeBefore = busRoutes.size();
    busRouteRepository.removeAll();
    int sizeAfter = busRouteRepository.getAll().size();
    Assert.assertTrue(sizeBefore > 0);
    Assert.assertTrue(sizeAfter == 0);
  }
}
