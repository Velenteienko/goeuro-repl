package com.omi.oi.goeuro.busroute;

import java.net.URI;

import org.hamcrest.core.Is;
import org.hamcrest.core.IsEqual;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import com.omi.oi.goeuro.busroute.dto.BusRouteResponse;
import com.omi.oi.goeuro.busroute.exception.ErrorResponse;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class BusrouteApplicationTest {

	@Autowired
	private TestRestTemplate rest;

	@Autowired
	private ApplicationContext ctx;

	private final String BASE_URL = "http://localhost:8088/";
	private final String PATH = "/api/direct";

	@Before
	public void setup() throws Exception {
		CommandLineRunner runner = ctx.getBean(CommandLineRunner.class);
		runner.run ("data/busroutesdata.txt");
	}

	@Test
	public void testReturnsOKAndDirectRouteExists() throws Exception {

		URI targetUrl = UriComponentsBuilder.fromUriString(BASE_URL)
				.path(PATH)
				.queryParam("dep_sid", 1)
				.queryParam("arr_sid", 2)
				.build()
				.toUri();

		ResponseEntity<BusRouteResponse> response = rest.getForEntity(targetUrl, BusRouteResponse.class);

		Assert.assertThat(response.getStatusCode(), Is.is(IsEqual.equalTo(HttpStatus.OK)));
		BusRouteResponse responseBody = response.getBody();
		Assert.assertTrue(responseBody.getRouteExists());
	}

	@Test
	public void testReturnsOKAndDirectRouteDoesNotExists() throws Exception {

		URI targetUrl = UriComponentsBuilder.fromUriString(BASE_URL)
				.path(PATH)
				.queryParam("dep_sid", 0)
				.queryParam("arr_sid", 0)
				.build()
				.toUri();

		ResponseEntity<BusRouteResponse> response = rest.getForEntity(targetUrl, BusRouteResponse.class);

		Assert.assertThat(response.getStatusCode(), Is.is(IsEqual.equalTo(HttpStatus.OK)));
		BusRouteResponse responseBody = response.getBody();
		Assert.assertFalse(responseBody.getRouteExists());
	}

	@Test
	public void testReturnsNOKAndParamsAreNotCorrect() throws Exception {

		URI targetUrl = UriComponentsBuilder.fromUriString(BASE_URL)
				.path(PATH)
				.queryParam("dep_sid", "one")
				.queryParam("arr_sid", "two")
				.build()
				.toUri();

		ResponseEntity<ErrorResponse> response = rest.getForEntity(targetUrl, ErrorResponse.class);

		Assert.assertThat(response.getStatusCode(), Is.is(IsEqual.equalTo(HttpStatus.BAD_REQUEST)));
		ErrorResponse responseBody = response.getBody();
		Assert.assertTrue(responseBody.getStatusCode() == 400);
	}

	@Test
	public void testReturnsNOKAndParamsAreNull() throws Exception {

		URI targetUrl = UriComponentsBuilder.fromUriString(BASE_URL)
				.path(PATH)
				.queryParam("dep_sid", null)
				.queryParam("arr_sid", null)
				.build()
				.toUri();

		ResponseEntity<ErrorResponse> response = rest.getForEntity(targetUrl, ErrorResponse.class);

		Assert.assertThat(response.getStatusCode(), Is.is(IsEqual.equalTo(HttpStatus.BAD_REQUEST)));
		ErrorResponse responseBody = response.getBody();
		Assert.assertTrue(responseBody.getStatusCode() == 400);
	}


}
