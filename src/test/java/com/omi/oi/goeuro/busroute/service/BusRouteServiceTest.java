package com.omi.oi.goeuro.busroute.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.omi.oi.goeuro.busroute.importer.BusRoutesLocalFileImporter;
import com.omi.oi.goeuro.busroute.service.impl.BusRouteServiceImpl;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class BusRouteServiceTest {

  @Autowired
  private BusRoutesLocalFileImporter importer;

  @Autowired
  private BusRouteServiceImpl busRouteService;

  @Before
  public void setup() throws Exception {
    importer.loadDataFile("data/busroutesdata.txt");
  }

  @Test
  public void testAreRoutesReturnTrueDirectDirections() throws Exception {
    Assert.assertTrue(busRouteService.isDirectRoute(1,2));
    Assert.assertTrue(busRouteService.isDirectRoute(3,6));
    Assert.assertTrue(busRouteService.isDirectRoute(2,4));
  }

  @Test
  public void testNoDirectDirection() throws Exception {
    Assert.assertFalse(busRouteService.isDirectRoute(1,9));
  }
}
