package com.omi.oi.goeuro.busroute.exception;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomBusRouteControllerException extends ResponseEntityExceptionHandler {

  @Value("${bus.route.error.internal}")
  private String INTERNAL_ERROR;


  @ExceptionHandler(BusRouteException.class)
  public final ResponseEntity<ErrorResponse> handleAppErrors(BusRouteException e, WebRequest request) {
    ErrorResponse errorResponse = new ErrorResponse(400, e.getMessage());
    return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
  }
}
