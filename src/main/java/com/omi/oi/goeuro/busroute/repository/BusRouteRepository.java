package com.omi.oi.goeuro.busroute.repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.omi.oi.goeuro.busroute.exception.BusRouteException;
import com.omi.oi.goeuro.busroute.model.BusRoute;

@Repository
public class BusRouteRepository {

  private List<BusRoute> busRoutes = new ArrayList<>();

  public List<BusRoute> getAll() {
    return Collections.unmodifiableList(busRoutes);
  }

  public BusRoute getById(int id) {
    return busRoutes.stream().filter(br -> br.getId() == id).collect(toSingleObject());
  }

  public void add(int routeId, Set<Integer> stations) throws BusRouteException {
    if (isRouteAlreadyExists(routeId)) {
      throw new BusRouteException("Route already exists for this set of stations");
    }
    BusRoute busRoute = new BusRoute(routeId, stations);
    busRoutes.add(busRoute);
  }


  public void removeAll() {
    busRoutes.clear();
  }

  private boolean isRouteAlreadyExists(int routeId) {
    return busRoutes.stream().anyMatch(brId -> brId.getId() == routeId);
  }

  private <T> Collector<T, ?, T> toSingleObject() {
    return Collectors.collectingAndThen(
        Collectors.toList(),
        list -> {
          if (list.size() != 1) {
            throw new IllegalStateException("More that 1 element is present");
          }
          return list.get(0);
        }
    );
  }
}
