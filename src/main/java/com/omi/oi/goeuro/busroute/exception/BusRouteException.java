package com.omi.oi.goeuro.busroute.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BusRouteException extends Exception {
  public BusRouteException() {
  }

  public BusRouteException(String message) {
    super(message);
  }
}
