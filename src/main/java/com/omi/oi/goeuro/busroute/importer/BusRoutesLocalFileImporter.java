package com.omi.oi.goeuro.busroute.importer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import com.omi.oi.goeuro.busroute.repository.BusRouteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.omi.oi.goeuro.busroute.exception.BusRouteException;

import javax.validation.constraints.NotNull;

@Component
public class BusRoutesLocalFileImporter {

  private final Logger logger = LoggerFactory.getLogger(BusRoutesLocalFileImporter.class);

  @Value("${file.route.minroutes}")
  private int MIN_STATION_PER_ROUTE;

  @Value("${file.route.header.count.index}")
  private int ROUTE_HEADER_COUNT;

  @Value("${file.route.station.index}")
  private Integer START_ROUTE_INDEX;

  private int totalRouteCount;

  @Autowired
  BusRouteRepository busRouteRepository;

  public void loadDataFile(@NotNull String pathToFile) throws BusRouteException {
    busRouteRepository.removeAll();
    try (Reader reader = new FileReader(pathToFile); BufferedReader bufferedReader = new BufferedReader(reader)) {
      extractRoutes(bufferedReader);
    } catch (IOException e) {
      logger.error("File not found by path {0}", pathToFile);
      throw new BusRouteException("Bus Route data file not found");
    } catch (BusRouteException e) {
      logger.error(e.getMessage());
      busRouteRepository.removeAll();
    }
  }


  private void extractRoutes(BufferedReader reader) throws IOException, BusRouteException {
    int routeCount;
    int linesCounter = 0;
    String line = reader.readLine();
    if (line == null) {
      throw new BusRouteException("Routes amount is missing in header");
    }
      List<Integer> routesCount = splitRows(line);
      if (routesCount.size() != 0) {
        routeCount = routesCount.get(0);
        String routeLine;
        while((routeLine = reader.readLine()) != null) {
          linesCounter++;
          List<Integer> busRoutes = splitRows(routeLine);
          int routeId = busRoutes.get(0);
          List<Integer> routes = busRoutes.subList(START_ROUTE_INDEX, busRoutes.size());
          if (routes.size() < MIN_STATION_PER_ROUTE) {
            logger.warn("Skip route id {0}. The count of stations less that {1}", routeId, MIN_STATION_PER_ROUTE);
            continue;
          }
          busRouteRepository.add(routeId, new HashSet<>(routes));
        }
        if (routeCount != linesCounter) {
          throw new BusRouteException("Amount of routes does not match: Routes "+routeCount+" actual "+linesCounter);
        }
      } else {
        logger.error("Cannot initialize repository. Routes are missing");
        throw new BusRouteException("Cannot initialize repository. Routes are missing");
      }
  }

  private List<Integer> splitRows(String row) throws BusRouteException {
    List<Integer> rowSet = new ArrayList<>();
    String[] rawRow = row.split(" ");
    for (int i = 0; i < rawRow.length; ++i) {
      try {
        rowSet.add(Integer.valueOf(rawRow[i]));
      } catch (NumberFormatException e) {
        throw new BusRouteException(
            "Cannot parse bus routes data: wrong data format. The route/station must be digits only");
      }
    }
    return rowSet;
  }

}
