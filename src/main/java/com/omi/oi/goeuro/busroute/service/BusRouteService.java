package com.omi.oi.goeuro.busroute.service;

public interface BusRouteService {

  boolean isDirectRoute(int depId, int arrId);

}
