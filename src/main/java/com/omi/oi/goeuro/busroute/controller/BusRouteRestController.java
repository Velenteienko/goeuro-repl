package com.omi.oi.goeuro.busroute.controller;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.omi.oi.goeuro.busroute.dto.BusRouteResponse;
import com.omi.oi.goeuro.busroute.exception.BusRouteException;
import com.omi.oi.goeuro.busroute.service.BusRouteService;

@RestController
public class BusRouteRestController {

  @Autowired
  BusRouteService busRouteService;

  @RequestMapping(method = RequestMethod.GET, value = "/api/direct", produces = "application/json")
  @ResponseBody
  public ResponseEntity<BusRouteResponse> findDirectConnection(@NotNull @RequestParam(value = "dep_sid") String depID,

                                                               @NotNull @RequestParam(value = "arr_sid") String arrID) throws BusRouteException {
    int dep, arr;
    checkEmptyParams(depID, arrID);
    try {
      dep = Integer.valueOf(depID);
      arr = Integer.valueOf(arrID);
    } catch (NumberFormatException e) {
      throw new BusRouteException("Some parameters are not a digits [dep_sid:"+depID+ " arr_sid:"+arrID+"]");
    }
    BusRouteResponse response = new BusRouteResponse();
    response.setDepId(dep);
    response.setArrId(arr);
    response.setRouteExists(false);
    if (busRouteService.isDirectRoute(dep, arr)) {
      response.setRouteExists(true);
    }
    return new ResponseEntity<>(response, HttpStatus.OK);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/api/error", produces = "application/json")
  @ResponseBody
  public ResponseEntity<BusRouteResponse> checkAdviceHandler() throws BusRouteException {
    checkEmptyParams("");
    return new ResponseEntity<>(new BusRouteResponse(0,0,false), HttpStatus.OK);
  }

  private void checkEmptyParams(String ... params) throws BusRouteException {
    for(String param : params) {
      if (StringUtils.isBlank(param)) {
        throw new BusRouteException("Cannot execute request. Parameter is empty");
      }
    }
  }
}

