package com.omi.oi.goeuro.busroute.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BusRouteResponse {

  private Integer depId;
  private Integer arrId;
  private Boolean isRouteExists;

  public BusRouteResponse() {}

  public BusRouteResponse(Integer depId, Integer arrId, Boolean isRouteExists) {
    this.depId = depId;
    this.arrId = arrId;
    this.isRouteExists = isRouteExists;
  }

  @JsonProperty("dep_sid")
  public Integer getDepId() {
    return depId;
  }

  public void setDepId(Integer depId) {
    this.depId = depId;
  }

  @JsonProperty("arr_sid")
  public Integer getArrId() {
    return arrId;
  }

  public void setArrId(Integer arrId) {
    this.arrId = arrId;
  }

  @JsonProperty("direct_bus_route")
  public Boolean getRouteExists() {
    return isRouteExists;
  }

  public void setRouteExists(Boolean routeExists) {
    isRouteExists = routeExists;
  }
}
