package com.omi.oi.goeuro.busroute.model;

import java.util.Collections;
import java.util.Set;

import com.omi.oi.goeuro.busroute.exception.BusRouteException;

public class BusRoute {

  private Integer id;
  private Set<Integer> stationsSet;

  public BusRoute(Integer routeId, Set<Integer> stations) throws BusRouteException {
    if (stations == null | stations.isEmpty()) {
      throw new BusRouteException("Route set cannot be null or empty");
    }
    this.id = routeId;
    this.stationsSet = stations;
  }

  public Integer getId() {
    return this.id;
  }

  public Set<Integer> getRoutes() {
    return Collections.unmodifiableSet(this.stationsSet);
  }



}
