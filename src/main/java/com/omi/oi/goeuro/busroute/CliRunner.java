package com.omi.oi.goeuro.busroute;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;

import com.omi.oi.goeuro.busroute.importer.BusRoutesLocalFileImporter;
import org.springframework.stereotype.Component;

@Component
public class CliRunner implements CommandLineRunner {

  private final Logger logger = LoggerFactory.getLogger(CliRunner.class);

  @Autowired
  private BusRoutesLocalFileImporter busRoutesLocalFileImporter;

  @Override
  public void run(String... args) throws Exception {
    if (args.length > 0) {
      String filename = args[0];
      try {
        busRoutesLocalFileImporter.loadDataFile(filename);
        logger.info("Bus Route Data imported");
      } catch (Exception ex) {
        logger.error("Failed to import Bus Route Data. " + ex.getMessage());
      }
    }
  }
}
