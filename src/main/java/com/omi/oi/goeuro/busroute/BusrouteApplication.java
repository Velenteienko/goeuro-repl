package com.omi.oi.goeuro.busroute;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BusrouteApplication {

	public static void main(String[] args) {
		SpringApplication.run(BusrouteApplication.class, args);
	}

}
