package com.omi.oi.goeuro.busroute.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.omi.oi.goeuro.busroute.model.BusRoute;
import com.omi.oi.goeuro.busroute.repository.BusRouteRepository;
import com.omi.oi.goeuro.busroute.service.BusRouteService;
import org.springframework.stereotype.Service;

@Service
public class BusRouteServiceImpl implements BusRouteService {

  @Autowired
  private BusRouteRepository busRouteRepository;

  @Override
  public boolean isDirectRoute(int depId, int arrId) {

    List<BusRoute> busRoutes = busRouteRepository.getAll();
    List<Boolean> directRoutesAggree = new ArrayList<>();
    for (BusRoute busRoute : busRoutes) {
      boolean isDirectRouteExists = (busRoute.getRoutes().contains(depId) && busRoute.getRoutes().contains(arrId)) ? true : false;
      if (isDirectRouteExists) {
        directRoutesAggree.add(isDirectRouteExists);
      }
    }
    return directRoutesAggree.size() > 0;
  }
}
