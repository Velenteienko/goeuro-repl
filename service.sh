#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
JAR="$DIR/target/busroute-1.0.0-SNAPSHOT.jar"
NAME=busroute-service
LOG=$DIR/$NAME.log
SPRING_OPTS="--logging.file=$LOG"
RUN="java -jar $JAR"

FILE="$DIR/data/busroutesdata.txt"

PID="busrouteapp.pid"
PIDFILE=$DIR/$PID

start() {
        if [ -f $PID ]; then
            rm -f $PID
        fi
    local CMD="$RUN $FILE $SPRING_OPTS & echo \$!"
    sh -c "$CMD"
}

stop() {
    if [ -f $PID ]; then
        kill -15 $(cat $PID)
        rm -f $PID
    else
        kill -15 $(pgrep -f $JAR)
    fi

}

help() {
    echo "Usage: $0 [start|stop] [pathToDtaFile]"
}

case $1 in
    start)
        if [ -z "$2" ]; then
            help
        else
            FILE=$2
            start
        fi
        ;;
    stop)
        stop
        ;;
    *)
        help
        ;;
esac
